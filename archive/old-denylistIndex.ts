/*
 * Filtered media are hostnames and domains that are being filtered and will nt have
 */
const filteredMedia = {
    "pawoo.net": {
        source: "https://mastodon.online/about/more",
        reason: "Inappropriate content",
        includeSubdomains: false,
        filterFlages: {
            interstitialRedirect: true,
            hideThumbnails: true,
            robotsNoFollow: true
        },
        banhammerUser: {
            name: "Community",
            email: "noreply+community@mail.recaptime.tk",
            url: "https://gitlab.com/RecapTimeBot"
        }
    },
    "ngrok.io": {
        source: "https://mastodon.technology/about/more",
        reason: "Domain is an tunneling service",
        includeSubdomains: true,
        filterType: {
            interstitialRedirect: true,
            hideThumbnails: true,
            robotsNoFollow: true
        },
        banhammerUser: {
            name: "Community",
            email: "noreply+community@mail.recaptime.tk",
            url: "https://gitlab.com/RecapTimeBot"
        }
    }
};
const serversUnderObservation = {};
const limitedServers = {};
const suspendedServers = {};

module.exports = {
    filteredMedia,
    serversUnderObservation,
    limitedServers,
    suspendedServers,
}
