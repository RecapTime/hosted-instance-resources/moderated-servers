# Moderated Federated Servers

Index of fediverse servers under strict moderation blocks and filters. The block list data is sourced from different Mastodon homeservers, including mastodon.technology and mastodon.online/.social. We'll also try to support blocking Matrix homeservers in the future in the main server backend.

In the future, we'll allow instance admins to customize their denylist indeices by self-hosting the server and pointing their instance to it.

## Our federation policies

Federation policies, both applied to both recaptime.app and other servers we federate with, can be found on [the policies repository](https://gitlab.com/RecapTime/hosted-instance-reosurces/policy/tree/main/policies/latest-draft.md), but TL;DR:

* follow the Community Code of Conduct, which based on Contributor Convenant, which includes:
  * no racism, sexism, homophobia, transphobia, xenophobia, or casteism
  * no incitement of violence or promotion of violent ideologies
  * no harassment, dogpiling or doxxing of other users
* both US and PH laws are being applied, so no illegal content

## Programmatic Access

> **Heads up!** The backend API stuff is still WIP, and the backend server for this will be go online in the future.

You can query the status of an domain through our API server hosted on Railway at:

```txt
https://fediverse-servers-banlist-prod.up.railway.app/api/<status>/<domain>
```

where:

* `status` - can be `filter`, `monitoring`, `muted`, or `blocked`
* `domain`- An hostname without protocol, such as `mastodon.online`.

### Example Response

```json
{
  "ok": true,
  "results": [
    {
        "domain": "example.com",
        "source": "https://gitlab.com/RecapTime/hosted-instance-resources/moderated-servers/issues/1234",
        "reason": "Placeholder Here",
        "includeSubdomains": false,
        "filterFlages": {
            "interstitialRedirect": true,
            "hideThumbnails": true,
            "robotsNoFollow": true,
            "hiddenFromFeeds": false,
            "mutedUnlessFollowed": false,
            "federationBlock": false
        },
        "banhammerUser": {
            "name": "Community",
            "email": "noreply+community@mail.recaptime.tk",
            "url": "https://gitlab.com/RecapTimeBot",
        }
      },
      // ...
  ]
}
```