<!--
NOTES and REMINDERS:
* Please replace the placehlders below with your own values. Issues with the same boilerplate will be closed without notice.
-->

## Server Information

* **Fediverse instance server hostname**: hostname.federated.tld
* **Current status**: <Limited|Suspended>
* **Reason for appealing**: Explain how you improve moderation and site policies on your home server, or if you had been wrongfully banlisted.
* **Is limited/suspended in other servers, especially `mastdn.social` and `mastodon.online`?** If yes, mention atleast 3 homeservers (not essentially every homeserver):
* **Admin cntact**: We preferred email, Telegram or Matrix for communication purposes and when contacting other homeservers' staff. If you choose to be contacted through email, please use <https://mailhide.io/en> or similiar as we'll eventually make this issue non-confiential when the issue is resolved.

## Evidence of Improvement/Wrongful Reporting

* [ ] Instance blocked on other instances? Please tick this box if you directly contacted them to lift the ban/limit and upload the evidence in the evidence upload section below.
* [ ] Wrongfully reported? Please tick this box and provide evidence regarding that within our public moderation log at <https://recaptime.gitlab.io/advisories>.

### Upload your evidence (or its links) below only in this section

* Link to evidence 1
* Link to evidence 2

## Checklist

DO NOT EDIT THIS SECTION OR ANY OF ITS SUBSECTIONS! This will only used for monitoring status, so please don't mindlessly tick anything without being said by our robots or any of our humans.

### Issue Submitter/Reporter

Please tick these boxes in order for us to process your appeal.

* [ ] I herby confirm that I'm an home server adnim/mod or an legal representative of them and has authority to request an appeal for my home server being limited/suspended.
* [ ] I agree that my contact information will be shared with other homeserver admins/mods in purpose of contacting me regarding this issue. I also agree that this issue will be marked as non-confidential once resolved.

### Squad Member / Community Mod

* [ ] Do an fact-check on submitted evidences if real and not faking it
* [ ] Contact home server admins to confirm if this homeserver's admin requested for limit/suspension lift
  * [ ] Check if atleast 75% of homeservers can reach the requested homeserver, through this might be tricky, so we'll only check on few sampled homeservers instead.
* [ ] Create an merge request removing its entry from `server/entries/suspsendedServers.json` or `server/entries/limited.json` and assign the issue reporter as MR assignee and @RecapTime/squad as MR reviewer.
  * Once merged, close this issue as resolved and lock down.

/confidential