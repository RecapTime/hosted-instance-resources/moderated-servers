<!--
NOTES and REMINDERS:
* First, if an site is deceptive, report to https://safebrowsing.google.com/safebrowsing/report_phish/ and https://www.microsoft.com/en-us/wdsi/support/report-unsafe-site/. This way, browsers that supports Google Safe Browsing will know to block that site. If you also report it to Microsoft too, that site can be also blocked on Office apps, Outlook, Bing and Edge.
* Second, check server/entries/filteredMedia.json in main branch if that site is filtered on our instances and through API. Also check server/entries/filteredDomainTlds.json in that branch too if that domain's TLD is also filtered too.
* As a last reminder, check if there's existing issues, especially the ones with ~"request-status::accepted" label.
-->

## Request form

* **Site URL**: cursed.domain.tld
* **Reason**: Please tick anything that applies.
  * [ ] Phlishing, malware, or other online threats
  * [ ] Violation of US/EU/PH laws, particularly copyright/DMCA
  * [ ] Ad spam
  * [ ] Possible trademark violation
  * [ ] Home of bot accounts
* **Should include subdomains also?**: <Yes or no>